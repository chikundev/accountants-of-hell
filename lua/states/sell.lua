-- chikun :: 2016
-- Selling state


-- Temporary state, removed at end of script
local sell_state = { }
local down = 0

-- On state create
function sell_state:create()

	-- Function to return to play state
	local gotoPlay = function() state.change(states.play) end

	-- Function to sell a soul
	local sellSoul = function()

		-- Retrieve the X and Y positions of the mouse
		local mouse_x, mouse_y = getMousePositions()

		-- Work out where in the x field is the soul
		local x_pos = math.floor(mouse_x / 252) + 1

		-- Work out where in the y field is the soul
		local y_pos = math.floor(mouse_y / 335) - 1

		local soul_clicked = x_pos + y_pos * 5 + down * 5

		local soul = player.list[1].souls[soul_clicked]

		local price = 0

		for key, sensitivity in ipairs(soul.sensitivity) do

			price = price + math.abs(sensitivity.level) * sensitivity.type.IFL
		end

		price = price * soul:getImpurity()

		player.list[1].money = player.list[1].money + price
		table.insert(player.list[2].souls, soul)
		table.remove(player.list[1].souls, soul_clicked)
	end

	sell_clicker = ClickManager()

	-- Button to return to play state
	sell_clicker:addRectangle('playReturn', GAME_WIDTH - 64, 0, 64, 48,
							  gotoPlay, nil, nil)

	for i = 1, 10 do

		local x_pos = 212 + 252 * ((i - 1) % 5)
		local y_pos = 334 + 335 * math.floor((i - 1) / 5)

		sell_clicker:addRectangle("Soul " .. i, x_pos, y_pos, 40, 20, sellSoul,
								  nil, nil)
	end
end


-- On state update
function sell_state:update(dt)

	sell_clicker:update(dt)
end


-- On state draw
function sell_state:draw()

	-- Draw background
	lg.setColor(255, 255, 255)
	lg.rectangle('fill', 0, 0, GAME_WIDTH, GAME_HEIGHT)

	-- Local scroll colouration
	local scroll = {
		red   = 184,
		green = 115,
		blue  = 51
		}


	for i = 1, 10 do

		if ((i + down * 5) <= #player.list[1].souls) then

			-- Set color to that of scroll color
			lg.setColor(scroll.red, scroll.green, scroll.blue)

			-- local scroll width and height
			local scroll_width = math.floor((GAME_WIDTH - 20) / 5)
			local scroll_height = math.floor((GAME_HEIGHT - 50) / 2)

			-- Create scrolls
			lg.rectangle('fill', 10 + scroll_width * ((i - 1) % 5),
				50 + scroll_height * math.floor((i - 1) / 5), scroll_width - 5,
				scroll_height - 15)

			-- Reset drawing color to black
			lg.setColor(0, 0, 0)

			-- Create empty text
			local text = ''

			-- Set current soul to the ith soul in list
			local soul = player.list[1].souls[i]

			-- Call up souls name
			local name = soul:getName()

			-- Add soul name to text
			text = text .. name

			-- Draw soul name
			lg.print(text, 15 + scroll_width * ((i - 1) % 5), 40 + scroll_height
					 * math.floor((i - 1) / 5))

			-- Reset text to blank
			text = ''

			-- Call up amount of Impurities
			local impurity = soul:getImpurity()

			-- Add impurity count to text
			text = text .. 'Impurities: ' .. impurity

			-- Write impurity count
			lg.print(text, 15 + scroll_width * ((i - 1) % 5), 76 + scroll_height
					 * math.floor((i - 1) / 5))

			-- Set font to table font
			lg.setFont(font.table)

			for key, sensitivity in ipairs(soul.sensitivity) do

				-- Empty text
				text = ''

				-- Add sensitivity name to text
				text = text .. sensitivity.type.name

				-- Figure out is sensitivity is phobia or philia
				if sensitivity.level >= 0 then

					-- State if phobia
					text = text .. 'phobia: '
				else

					-- State if philia
					text = text .. 'philia: '
				end

				-- Add sensitivity level to text
				text = text .. math.abs(sensitivity.level) .. '\n'

				-- Write the current sensitivity
				lg.print(text, 15 + scroll_width * ((i - 1) % 5), 128 + (32 *
					((key - 1) % 5 ) + scroll_height * math.floor((i - 1) / 5)))
			end

			-- Reset font to main font
			lg.setFont(font.main)

			-- Empty text
			text = ''

			-- create a variable to hold price of soul
			local price = 0

			for key, sensitivity in ipairs(soul.sensitivity) do

				-- Add on the sensitivity level and IFL to each soul
				price = price + math.abs(sensitivity.level)
								* sensitivity.type.IFL
			end

			-- Finish calculating price
			price = price * soul:getImpurity()

			-- Set default units
			local unit = 'Thou'

			-- If price is 10 mill or higher
			if price >= 10000 then

				-- Decrease holder of price by 1 thousand
				price = price / 1000

				-- Change units to millions
				unit = 'Mill'
			end

			-- Set text to be the price of the soul
			text = text .. '$' .. price .. ' ' .. unit

			-- Write the price of soul at bottom of relevant scroll
			lg.print(text, 10 + scroll_width * ((i - 1) % 5),
				 scroll_height - 28 + scroll_height * math.floor((i - 1) / 5))

			-- Write sell at bottom of each scroll
			lg.print("sell", scroll_width - 40 + scroll_width * ((i - 1) % 5),
				 scroll_height - 28 + scroll_height * math.floor((i - 1) / 5))
		end
	end

	-- Reset color yo black
	lg.setColor(0, 0, 0)

	-- Write place for back button in soul
	lg.print("back", GAME_WIDTH - 50, 0)

	-- Drawing money
	local money = player.list[1].money
	local text = " Thousand"

	-- Set color for text
	if (money < 0) then

		lg.setColor(128, 0, 0)
	else

		lg.setColor(0, 0, 0)
	end

	-- Scale money into billions
	if (money <= -10000 or money >= 10000) then
		money = money / 1000
		text = " Million"

		if (money <= -10000 or money >= 10000) then
			money = money / 1000
			text = " Billion"
		end
	end

	-- Actually print the money amount
	lg.print("$" .. money .. text, GAME_WIDTH / 2 - 20, 0)
end


-- On state kill
function sell_state:kill() end


-- Transfer data to state loading script
return sell_state
