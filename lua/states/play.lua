-- chikun :: 2015
-- Play state


-- Temporary state, removed at end of script
local play_state = { }

-- Timer for in game clock to set times for buying and selling
clock = 0

-- Speed factor for speeding up and slowing down game
speed_factor = 1

-- On play state create
function play_state:create()

	-- Functions for returning time to normal
	local speed_factor_default = function() speed_factor = 1 end

	-- Function for doubling game speed
	local speed_factor_fast = function() speed_factor = 2 end

	-- Function for Quadrupling game speed (4x)
	local speed_factor_super_fast = function() speed_factor = 4 end

	-- Function for slowing game speed (1/2)
	local speed_factor_slow = function() speed_factor = 0.5 end

	-- Function for going to the pause state
	local gotoPause = function() state.change(states.pause) end

	-- Edited go to shop to go only to either buying or selling
	local gotoShop  = function()

		-- If within first hour of work day
		if (clock <= 60) then

			-- Go to shop
			state.change(states.shop)

		-- If within last hour of work day
		elseif (clock >= 900 and clock < 960) then

			-- Go to selling
			state.change(states.sell)
		end
	end

	-- Function for going to the soul door screen
	local gotoSoul  = function()

		-- Set current soul to the doors souls
		for key, souls in ipairs(player.list[1].souls) do

			-- Compare if soul clicked on is in player table
			if souls.object == play_clicker.pressed_object then

				-- Set current soul to one within players ownership
				current_soul = souls
			end
		end

		-- Move to soul doors state
		state.change(states.soul_door)
	end


	-- Create player
	player.add('Bradley')
	player.add('Shop of Souls')

	-- Generate 1 - 100 souls
	if #player.list[2].souls < 5 then
		repeat

			soul.add(name.generate(), math.random(100), 'Shop of Souls')
		until (#player.list[2].souls == 100)
	end

	-- Create play's click manager
	play_clicker = ClickManager()

	-- Slow Speed Object
	play_clicker:addRectangle('slow', 96, 0, 32, 32, speed_factor_slow, nil,
								nil)

	-- Normal Speed Object
	play_clicker:addRectangle('normal', 128, 0, 32, 32, speed_factor_default,
								nil, nil)

	-- Fast Speed Object
	play_clicker:addRectangle('fast', 96, 32, 32, 32, speed_factor_fast, nil,
								nil)

	-- Super Fast Speed Object
	play_clicker:addRectangle('super_fast', 128, 32, 32, 32,
								speed_factor_super_fast, nil, nil)

	-- Pause object
	play_clicker:addCircle('pause', GAME_WIDTH - 32, 32, 32,
						   gotoPause, nil, nil)

	-- Shop object
	play_clicker:addCircle('shop', GAME_WIDTH - 96, 32, 32, gotoShop, nil, nil)


	-- Quick variables for positioning the doors for souls
	local x_pos, y_pos = 0, 0

	for i = 1, #player.list[1].souls do

		-- create local door to be where the clickable door is
		local door = {
			x = 150 + (250 * x_pos),
			y = 714 - (155 * (y_pos + 1)),
			w = 100,
			h = 135
		}

		-- Creating the clickable rectangle
		player.list[1].souls[i].object =
			play_clicker:addRectangle(player.list[1].souls[i]:getName(),
			door.x, door.y, door.w, door.h, gotoSoul, nil, nil)

		-- Moving where next will spawn
		x_pos = x_pos + 1

		-- After 5 doors in a line, move to next
		if (x_pos >= 5) then

			y_pos = y_pos + 1
			x_pos = 0
		end
	end
end


-- On play state update
function play_state:update(dt)

	-- Souls update cycle
	soul.update(dt)

	-- Countdown to remove a soul
	soul.countDown(dt)

	if (state.current == states.play) then

		-- Update play's click manager
		play_clicker:update(dt)
	end

	-- Count up clock while in play or soul door
	clock = clock + dt

	-- If clock ticks over a new day
	if clock >= 1440 then

		-- Reset clock
		clock = clock - 1440
	end
end


-- On play state draw
function play_state:draw()

	-- Settings

	-- Set drawing colour
	lg.setColor(255, 255, 255)

	-- Draw background image
	lg.draw(gfx.placeholders.bg, 0, 0)

	-- Use main font
	lg.setFont(font.main)


	-- Make door for each soul
	for i = 1, #player.list[1].souls do

		-- Reset basic draw color to black
		lg.setColor(255, 255, 255)

		local door = player.list[1].souls[i].object

		-- Draw door
		lg.draw(gfx.placeholders.door, door.x, door.y)

		lg.setColor(0, 0, 0)

		-- Get information on each soul
		local ltext = player.list[1].souls[i]:getName()
		local text = ""

		-- Display information on each soul next to door
		for i in string.gmatch(ltext, "%S+") do

			text = text .. i .. "\n"
		end

		lg.print(text, door.x - 125, door.y)

		-- Find if there is a sensitivity that is going up or down
		local sensitivity_exp_dif = 2

		for key, sensitivity in ipairs(player.list[1].souls[i].sensitivity) do

			if (not (sensitivity.exp_dif == 0)) then

				sensitivity_exp_dif = math.min(sensitivity_exp_dif,
											   sensitivity.exp_dif)
			end
		end

		-- If none are going up or down then default to none
		if sensitivity_exp_dif == 2 then

			sensitivity_exp_dif = 0
		end

		-- Determine color of light beside door
		if (sensitivity_exp_dif == 1) then

			lg.setColor(128, 255, 128)
		elseif (sensitivity_exp_dif == -1) then

			lg.setColor(255, 128, 128)
		else

			lg.setColor(255, 255, 255)
		end

		-- Draw light beside door
		lg.draw(gfx.placeholders.light, door.x - 25, door.y + 50, 0, 5 / 8)

		-- Display light above door to show if soul is earning or losing money
		if (player.list[1].souls[i].earn_money < 0) then

			lg.setColor(255, 128, 128)
		elseif (player.list[1].souls[i].earn_money > 0) then

			lg.setColor(128, 255, 128)
		else

			lg.setColor(255, 255, 255)
		end

		-- Draw light
		lg.draw(gfx.placeholders.light, door.x + door.w / 2 - 4, door.y - 18 ,
				0, 0.5)
	end


	-- Draw buttons
	lg.setColor(255, 255, 255)
	lg.draw(gfx.buttons.shop, GAME_WIDTH - 128, 0)
	lg.draw(gfx.buttons.pause, GAME_WIDTH - 64, 0)

	-- Drawing money
	local money = player.list[1].money
	local text = " Thousand"

	-- Set color for text
	if (money < 0) then

		lg.setColor(128, 0, 0)
	else

		lg.setColor(0, 0, 0)
	end

	-- Scale money into billions
	if (money <= -10000 or money >= 10000) then
		money = money / 1000
		text = " Million"

		if (money <= -10000 or money >= 10000) then
			money = money / 1000
			text = " Billion"
		end
	end

	-- Actually print the money amount
	lg.print("$" .. money .. text, GAME_WIDTH / 2 - 20, 20)

	-- Reset color to black
	lg.setColor(0, 0, 0)

	-- Draw area for clock
	lg.rectangle('fill', 0, 0, 72, 64)

	-- Set color to red
	lg.setColor(255, 0, 0)

	-- Calculate current game hour
	local hour = math.floor(clock / 60) + 7

	-- Swing cock around to next day if past midnight
	if hour >= 24 then
		hour = hour - 24
	end

	-- Calculate current game minute
	local minute = math.floor(clock % 60)

	-- If minute is less than ten
	if minute < 10 then

		-- Add the 0 before the number to be like analogue clock
		minute = '0' .. minute
	end

	-- Print current time
	lg.print(hour .. ':' .. minute, 0, 0)

	-- Reset color to black
	lg.setColor(255, 255, 255)

	-- Draw buttons for speed changing
	lg.draw(gfx.buttons.slow, 96, 0)
	lg.draw(gfx.buttons.normal, 128, 0)
	lg.draw(gfx.buttons.fast, 96, 32)
	lg.draw(gfx.buttons.superfast, 128, 32)
end


-- On play state kill
function play_state:kill()

	player_clicker = nil
end


-- Transfer data to state loading script
return play_state
