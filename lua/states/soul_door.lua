-- chikun :: 2014
-- Soul door state


-- Temporary state, removed at end of script
local soul_state = { }


-- On state create
function soul_state:create()

	-- Create soul doors clicker
	soul_door_clicker = ClickManager()

	local function backToPlay()

		state.set(states.play)
	end

	local function goUpSouls()

		-- Look through all souls player owns
		for key, soul in ipairs(player.list[1].souls) do

			-- If found current soul
			if (soul.name == current_soul.name) then

				-- If there is a soul in list five higher
				if (key + 5 <= #player.list[1].souls) then

					-- Change current soul to the one five higher
					current_soul = player.list[1].souls[key + 5]
				end

				break
			end
		end
	end

	local function goRightSouls()

		-- Look through all souls player owns
		for key, soul in ipairs(player.list[1].souls) do

			-- If found current soul
			if (soul.name == current_soul.name) then

				-- If there is a soul in list one higher
				if ((key + 1 <= #player.list[1].souls) and (key % 5 > 0)) then

					-- Change current soul to the one one higher
					current_soul = player.list[1].souls[key + 1]
				end

				break
			end
		end
	end

	local function goLeftSouls()

		-- Look through all souls player owns
		for key, soul in ipairs(player.list[1].souls) do

			-- If found current soul
			if (soul.name == current_soul.name) then

				-- If there is a soul in list one lower on same level
				if ((key - 1 > 0) and not (key % 5 == 1)) then

					-- Change current soul to the one one lower
					current_soul = player.list[1].souls[key - 1]
				end

				break
			end
		end
	end

	local function goDownSouls()

		-- Look through all souls player owns
		for key, soul in ipairs(player.list[1].souls) do

			-- If found current soul
			if (soul.name == current_soul.name) then

				-- If there is a soul in list five lower
				if (key - 5 > 0) then

					-- Change current soul to the one five lower
					current_soul = player.list[1].souls[key - 5]
				end

				break
			end
		end
	end

	-- Function to toggle fears
	-- Input:  None
	-- Output: None
	local function toggleFear()

		-- Get the current mouse positions
		local mouse_x, mouse_y = getMousePositions()

		-- Find out which fear is being toggled
		local fear_to_toggle = math.floor((mouse_y - 160) / 48)

		-- Way to shorten what the code says
		local sensitivity = current_soul.sensitivity[fear_to_toggle]
		-- Toggle fear
		sensitivity.active = not sensitivity.active

		player.list[1].money = player.list[1].money -
		                        (math.abs(sensitivity.type.IFL *
			                             40 * sensitivity.level) + 200)
	end

	-- Create button to return to play state
	soul_door_clicker:addCircle('Back', GAME_WIDTH - 64, 32, 32,
								backToPlay, nil, nil)

	-- Create button to go up 1 soul if possible
	soul_door_clicker:addRectangle('Up', GAME_WIDTH / 2 - 40, 10, 80, 20,
		                           goUpSouls, nil, nil)
	-- Create button to go right 1 soul if possible
	soul_door_clicker:addRectangle('Right', GAME_WIDTH - 30,
									GAME_HEIGHT / 2 - 40, 20, 80,
		                           goRightSouls, nil, nil)
	-- Create button to go left 1 soul if possible
	soul_door_clicker:addRectangle('Left', 10, GAME_HEIGHT / 2 - 40, 20, 80,
		                           goLeftSouls, nil, nil)
	-- Create button to go left 1 soul if possible
	soul_door_clicker:addRectangle('Down', GAME_WIDTH / 2 - 40,
									GAME_HEIGHT - 30, 80, 20,
		                           goDownSouls, nil, nil)

	for i = 1, 5 do

		y_pos = 160 + (i * 48)

		soul_door_clicker:addRectangle('Toggle Fear ' .. i, 50, y_pos + 16,
									   24, 24, toggleFear, nil, nil) --
	end
end


-- On state update
function soul_state:update(dt)

	-- Update clicker for soul door
	soul_door_clicker:update(dt)

	-- update background play state
	states.play:update(dt)
end


-- On state draw
function soul_state:draw()

	local text = ""
	-- Creating background
	lg.setColor(255, 255, 255)
	lg.draw(gfx.placeholders.bg, 0, 0)

	-- Create graphic for door
	lg.setColor(32, 32, 32)
	lg.rectangle("fill", GAME_WIDTH * 9 / 16, 80,
				 GAME_WIDTH * 3 / 8, GAME_HEIGHT - 160)

	-- Create graphic for light above door
	if (current_soul.earn_money < 0) then

		lg.setColor(255, 128, 128)
	elseif (current_soul.earn_money > 0) then

		lg.setColor(128, 255, 128)
	else

		lg.setColor(255, 255, 255)
	end

	lg.draw(gfx.placeholders.light, GAME_WIDTH * 3 / 4, 34)

	-- Navigation color
	lg.setColor(255,255,255)

	lg.circle('fill', GAME_WIDTH - 64, 32, 32, 100)    -- Button back
	lg.rectangle('fill', GAME_WIDTH / 2 - 40, 10, 80, 20)  -- Button up
	lg.rectangle('fill', GAME_WIDTH - 30, GAME_HEIGHT / 2 - 40, 20, 80)
	-- Button right
	lg.rectangle('fill', 10, GAME_HEIGHT / 2 - 40, 20, 80)  -- Button left
	lg.rectangle('fill', GAME_WIDTH / 2 - 40, GAME_HEIGHT - 30, 80, 20)
	-- Button down

	-- Write souls name
	lg.setColor(0, 0, 0)
	text = current_soul.name
	lg.print(text, 150, 130)
	text = "Impurities: " .. current_soul.impurity
	lg.print(text, 380, 130)

	-- Drawing all sensitivities texts
	for key, sensitivity in ipairs (current_soul.sensitivity) do

		-- Clear text
		local y_pos = 160 + (key * 48)
		text = ""

		-- Light to show if sensitivity experience is going up or down
		if (sensitivity.exp_dif < 0) then

			lg.setColor(255, 0, 0)
		elseif (sensitivity.exp_dif > 0) then

			lg.setColor(0, 255, 0)
		else

			lg.setColor(128, 128, 128)
		end

		lg.draw(gfx.placeholders.light, 37, y_pos + 22, 0, 0.5)

		-- Reset color to black
		lg.setColor(0, 0, 0)

		-- Temporary check box
		lg.rectangle('line', 55, y_pos + 16, 24, 24)

		if (sensitivity.active) then
			lg.print("A", 55, y_pos)
		else
			lg.print("X", 55, y_pos)
		end

		-- Add sensitivities name to text
		text = text .. sensitivity.type.name

		-- Test if phobia or philia then add it to the sensitivities name
		if (sensitivity.level >= 0) then

			text = text .. "phobia"
		else

			text = text .. "philia"
		end

		-- Print sensitivities name
		lg.print(text, 85, y_pos)

		text = ""
		-- Add level of sensitivity
		text = text .. math.abs(sensitivity.level) .. "\t"

		-- Calculate the cost
		local cost = math.abs(sensitivity.type.IFL * 40 *
		                      sensitivity.level) + 200

		-- Displaying of cost
		text = text .. "Cost: $" .. cost .. " thousand"

		-- Print level and cost of activation/disactivation
		lg.print(text, 280, y_pos)
	end

	-- Drawing money
	local money = player.list[1].money
	local text = " Thousand"

	-- Set color for text
	if (money < 0) then

		lg.setColor(128, 0, 0)
	else

		lg.setColor(0, 0, 0)
	end

	-- Scale money into billions
	if (money <= -10000 or money >= 10000) then
		money = money / 1000
		text = " Million"

		if (money <= - 10000 or money >= 10000) then
			money = money / 1000
			text = " Billion"
		end
	end

	-- Actually print the money amount
	lg.print("$" .. money .. text, 50, 80)

	-- Reset color to black
	lg.setColor(0, 0, 0)

	-- Draw area for clock
	lg.rectangle('fill', 0, 0, 72, 64)

	-- Set color to red
	lg.setColor(255, 0, 0)

	-- Calculate current game hour
	local hour = math.floor(clock / 60) + 7

	-- Swing cock around to next day if past midnight
	if hour >= 24 then
		hour = hour - 24
	end

	-- Calculate current game minute
	local minute = math.floor(clock % 60)

	-- If minute is less than ten
	if minute < 10 then

		-- Add the 0 before the number to be like analogue clock
		minute = '0' .. minute
	end

	-- Print current time
	lg.print(hour .. ':' .. minute, 0, 0)
end


-- On state kill
function soul_state:kill()

	-- Destroy soul door clicker
	soul_door_clicker = nil
end


-- Transfer data to state loading script
return soul_state
