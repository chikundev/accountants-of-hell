-- chikun :: 2014
-- Template state


-- Temporary state, removed at end of script
local new_state = { }


-- On state create
function new_state:create() end


-- On state update
function new_state:update(dt) end


-- On state draw
function new_state:draw() end


-- On state kill
function new_state:kill() end


-- Transfer data to state loading script
return new_state
