-- chikun :: 2015
-- Pause state


-- Temporary state, removed at end of script
local pause_state = { }


-- On pause state create
function pause_state:create()

	-- Save what the last state was
	saved_state = last_state

	-- Manages clicking in the pause state
	pause_clicker = ClickManager()

	-- Buttons which will be displayed in pause menu
	buttons = {
		resume   = pause_clicker:addRectangle('Resume',
											GAME_WIDTH / 2 - 120, 128, 240, 32,
			function()

				state.set(saved_state)
			end),

		saveload = pause_clicker:addRectangle('Save / Load',
											GAME_WIDTH / 2 - 120, 176, 240, 32,
			function()

				state.change(states.saveload)
			end),

		options  = pause_clicker:addRectangle('Options',
											GAME_WIDTH / 2 - 120, 224, 240, 32,
			function()

				state.change(states.options)
			end),

		quitgame = pause_clicker:addRectangle('Quit Game',
											GAME_WIDTH / 2 - 120, 272, 240, 32,
			function()

				love.event.quit()
			end)
	}
end


-- On pause state update
function pause_state:update(dt)

	-- Update pause click manager
	pause_clicker:update(dt)
end


-- On pause state draw
function pause_state:draw()

	-- Draw last state
	saved_state:draw()

	-- Draw alpha box
	lg.setColor(0, 0, 0, 192)
	lg.rectangle('fill', 0, 0, GAME_WIDTH, GAME_HEIGHT)

	-- Set main font
	lg.setFont(font.main)

	-- Iterate through all clickable objects
	for key, button in pairs(buttons) do

		-- Draw rectangle box
		lg.setColor(220, 155, 100, 128)
		lg.rectangle('fill', button.x, button.y, button.w, button.h)

		-- Draw title of box
		lg.setColor(255, 255, 255)
		lg.printf(button.name, button.x, button.y + button.h / 2 -
		          lg.getFont():getHeight() / 2, button.w, 'center')
	end

	-- Title of pause menu
	lg.setFont(font.header)
	lg.printf("GAME PAUSED", 0, 80, GAME_WIDTH, 'center')
end


-- On pause state kill
function pause_state:kill()

	pause_clicker = nil
	buttons = nil
	saved_state = nil
end


-- Transfer data to state loading script
return pause_state
