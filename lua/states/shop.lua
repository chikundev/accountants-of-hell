-- chikun :: 2015
-- Shop state


-- Temporary state, removed at end of script
local shop_state = { }
local list = { }

-- On shop state create
function shop_state:create()

	local gotoPlay = function() state.change(states.play) end
	local buySoul  = function()

		local mouse_x, mouse_y = getMousePositions()

		local soul_bought = math.floor((mouse_x - 212) / 252) +
							math.floor((mouse_y - 307) / 335) * 5

		if ((soul_bought == 0) or (soul_bought == 5)) then

			soul_bought = soul_bought + 5
		end

		local soul_bought_object = player.list[2].souls[list[soul_bought]]

		-- Local price for each soul
		local price = 0

		-- Sum all sensitivity levels to souls
		for key, sensitivity in ipairs(soul_bought_object.sensitivity) do
			price = price + math.abs(sensitivity.level)
		end

		price = price * soul_bought_object.impurity * 10

		player.list[1].money = player.list[1].money - price

		table.remove(player.list[2].souls, list[soul_bought])
		table.insert(player.list[1].souls, soul_bought_object)

		list = { }

		repeat
			local temp_num = math.random(1, #player.list[2].souls)
			local boolean = false

			for key , value in ipairs(list) do
				if (value == temp_num) then
					boolean = true
				end
			end

			if (boolean == false) then

				table.insert(list, temp_num)
			end
		until ((#list == 10) or (#list == #player.list[2].souls))

	end

	if (#player.list[2].souls > 0) then
		repeat
			local temp_num = math.random(1, #player.list[2].souls)
			local boolean = false

			for key , value in ipairs(list) do
				if (value == temp_num) then
					boolean = true
				end
			end

			if (boolean == false) then

				table.insert(list, temp_num)
			end
		until (#list == 10 or #list == #player.list[2].souls)
	end

	shop_clicker = ClickManager()

	shop_clicker:addRectangle('playReturn', GAME_WIDTH - 64, 0, 64, 48,
							  gotoPlay, nil, nil)

	for i = 1, #list do

		local x_pos = 212 + 252 * (i % 5)
		local y_pos = 307 + 335 * math.floor((i - 1) / 5)

		shop_clicker:addRectangle("Soul " .. i, x_pos, y_pos, 64, 48, buySoul,
								  nil, nil)
	end

end


-- On shop state update
function shop_state:update(dt)

	shop_clicker:update(dt)
end


-- On shop state draw
function shop_state:draw()

	-- Draw black background for shop
	lg.setColor(255, 255, 255)
	lg.rectangle('fill', 0, 0, 1280, 720)

	-- Local scroll colouration
	local scroll = {
		red   = 184,
		green = 115,
		blue  = 51
		}

	-- Iterate through chosen souls
	for i = 1, #list do

		-- Set color to that of the scrolls
		lg.setColor(scroll.red, scroll.green, scroll.blue)

		-- local scroll width and height
		local scroll_width = math.floor((GAME_WIDTH - 20) / 5)
		local scroll_height = math.floor((GAME_HEIGHT - 50) / 2)

		-- Create scrolls
		lg.rectangle('fill', 10 + scroll_width * (i % 5),
			50 + scroll_height * math.floor((i - 1) / 5), scroll_width - 5,
			scroll_height - 15)

		-- Reset drawing color to black
		lg.setColor(0, 0, 0)

		-- Create local text and extremely local name holder
		local text = ""

		text = player.list[2].souls[(list[i])].name

		-- Write text for name
		lg.print(text, 15 + scroll_width * (i % 5),
				 40 + scroll_height * math.floor((i - 1) / 5))

		-- Clear text
		text = ""

		-- Add total impurities to text
		text = "Impurity: ".. player.list[2].souls[(list[i])].impurity

		-- Write total Impurities
		lg.print(text, 10 + scroll_width * (i % 5),
				 76 + scroll_height * math.floor((i - 1) / 5))

		-- Change font to smaller font
		lg.setFont(font.table)

		-- Adding all sensitivities to text list
		for key, sensitivity in ipairs(
								player.list[2].souls[(list[i])].sensitivity) do

			-- Clear text for each sensitivity
			text = ""

			text = sensitivity.type.name

			-- Determine type of sensitivity
			if (sensitivity.level >= 0) then

				text = text .. "phobia: "
			else

				text = text .. "philia: "
			end

			-- Display level of sensitivity
			text = text .. math.abs(sensitivity.level)

			-- Write all Sensitivities
			lg.print(text, 15 + scroll_width * (i % 5), 128 + (32 *
					((key - 1) % 5 ) + scroll_height * math.floor((i - 1) / 5)))
		end

		-- Reset text and font
		lg.setFont(font.main)
		text = ""

		-- Local price for each soul
		local price = 0

		-- Sum all sensitivity levels to souls
		for key, sensitivity in ipairs(
								player.list[2].souls[(list[i])].sensitivity) do

			price = price + math.abs(sensitivity.level) * sensitivity.type.IFL
		end

		-- Calculate final price of soul
		price = price * player.list[2].souls[(list[i])].impurity * 10

		-- Assign default unit of thousands
		local units = "Thou"

		-- Check if unit needs to be millions
		if (price >= 10000) then

			price = price / 1000
			units = "Mill"
		end

		-- Add price to end of text list
		text = "$".. price .. units

		lg.print(text, 10 + scroll_width * (i % 5),
				 scroll_height - 28 + scroll_height * math.floor((i - 1) / 5))

		-- Set color to black
		lg.setColor(0, 0, 0)

		-- Drawing all text for each soul
		lg.print("buy", scroll_width - 40 + scroll_width * (i % 5),
				 scroll_height - 28 + scroll_height * math.floor((i - 1) / 5))
	end

	-- Set color to black
	lg.setColor(0, 0, 0)

	-- Draw back text
	lg.print("back", GAME_WIDTH - 50, 0)

	-- Drawing money
	local money = player.list[1].money
	local text = " Thousand"

	-- Set color for text
	if (money < 0) then

		lg.setColor(128, 0, 0)
	else

		lg.setColor(0, 0, 0)
	end

	-- Scale money into billions
	if (money <= -10000 or money >= 10000) then
		money = money / 1000
		text = " Million"

		if (money <= -10000 or money >= 10000) then
			money = money / 1000
			text = " Billion"
		end
	end

	-- Actually print the money amount
	lg.print("$" .. money .. text, GAME_WIDTH / 2 - 20, 0)
end


-- On shop state kill
function shop_state:kill()

	list = { }
end


-- Transfer data to state loading script
return shop_state
