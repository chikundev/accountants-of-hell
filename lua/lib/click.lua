-- chikun :: 2015
-- Click thing


function getMousePositions()

	return (lm.getX() - off_x) / scale, (lm.getY() - off_y) / scale
end


-- Click manager class
ClickManager = class(function(click)

		click.objects = { }

		local x, y = getMousePositions()

		click.position = {
			x = x,      y = y,
			x_prev = x, y_prev = y
		}

		click.initial = true
	end)


--[[
	On manager update.
	INPUT:	Delta time.
	OUTPUT:	Nothing.
]]
function ClickManager:update(dt)

	if (self.initial and lm.isDown(1)) then

		return 0
	end

	self.initial = nil


	-- Update click positon
	self.position.x, self.position.y = getMousePositions()

	-- If left button pressed
	if (lm.isDown(1)) then

		-- If pressed is already active...
		if (self.pressed) then

			-- If an object has been pressed
			if (self.pressed_object) then

				-- If that object has a while function
				if (self.pressed_object.f_while) then

					-- Activate the while function
					self.pressed_object.f_while:update(dt)
				end
			end

		else

			-- Iterate through all objects
			for k, object in ipairs(self.objects) do

				-- We have indeed pressed
				self.pressed = true

				-- If object is colliding with the click
				if (self:checkCollision(object)) then

					-- That object is the click object
					self.pressed_object = object

					-- If object has an on function
					if (self.pressed_object.f_on) then

						-- Activate the on function
						self.pressed_object.f_on()
					end

					-- Break out of the loop
					break
				end
			end
		end

	-- If left button not pressed, but click is still pressed
	elseif (self.pressed) then

		-- If an object has been pressed
		if (self.pressed_object) then

			-- If object has an off function
			if (self.pressed_object.f_off) then

				-- Activate off function
				self.pressed_object.f_off()
			end
		end

		-- Reset pressed state
		self.pressed = false
	end

	-- Update click previous positions
	self.position.x_prev = self.position.x
	self.position.y_prev = self.position.y
end


--[[
	Check for collision between click and object.
	INPUT:	Object to check for collision.
	OUTPUT:	Whether the collision is occuring.
]]
function ClickManager:checkCollision(obj)

	-- If object is a circle
	if (obj.r) then

		-- Return whether the click collides with the circle
		return	math.distance(self:getX(), self:getY(), obj.x, obj.y) < obj.r
	elseif (obj.w and obj.h) then

		-- Return if click collides with a rectangle
		return	obj.x < self:getX() and self:getX() < obj.x + obj.w and
				obj.y < self:getY() and self:getY() < obj.y + obj.h
	else

		return false
	end
end


--[[
	Add circle clicker to ClickManager's objects table.
	INPUT:  name, x, y, radius and functions to activate.
	OUTPUT: Newly created click object.
]]
function ClickManager:addCircle(name, x, y, r, f_on, f_while, f_off)

	table.insert(self.objects, {
		name = name,
		x = x,
		y = y,
		r = r,
		f_on = f_on,
		f_while = f_while,
		f_off = f_off
	})

	return self.objects[#self.objects]
end

--[[
	Add a rectangle clicker to ClickManager's objects table.
	INPUT:  name, x, y, width, height and functions to activate.
	OUTPUT: Newly created click object.
]]
function ClickManager:addRectangle(name, x, y, w, h, f_on, f_while, f_off)

	table.insert(self.objects, {
		name = name,
		x = x,
		y = y,
		w = w,
		h = h,
		f_on = f_on,
		f_while = f_while,
		f_off = f_off
	})

	return self.objects[#self.objects]
end

--[[
	Remove clickable object.
	INPUT:	name of object to remove.
	OUTPUT:	Nothing.
]]
function ClickManager:removeObject(name) end


--[[
	Returns the x position of the ClickManager.
	INPUT:	Nothing.
	OUTPUT:	x position of ClickManager.
]]
function ClickManager:getX()

	return self.position.x
end


--[[
	Returns the y position of the ClickManager.
	INPUT:	Nothing.
	OUTPUT:	y position of ClickManager.
]]
function ClickManager:getY()

	return self.position.y
end


--[[
	Returns the previous x position of the ClickManager.
	INPUT:	Nothing.
	OUTPUT:	Previous x position of ClickManager.
]]
function ClickManager:getXPrev()

	return self.position.x_prev
end


--[[
	Returns the previous y position of the ClickManager.
	INPUT:	Nothing.
	OUTPUT:	Previous y position of ClickManager.
]]
function ClickManager:getYPrev()

	return self.position.y_prev
end
