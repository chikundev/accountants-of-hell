-- chikun :: 2015
-- Generate a random name


-- Holds functions for names
name = { }


--[[
	Randomly generate a full name
	INPUT:	Nothing.
	OUTPUT:	A randomly generated name.
]]
function name.generate()

	return names.first[math.random(1, #names.first)] .. " " ..
		 	names.last[math.random(1, #names.last)]
end

-- List of names that can be used
names = {
	first = {
		"Bradley",
		"Josef",
		"Jackson",
		"Aiden",
		"Liam",
		"Lucas",
		"Noah",
		"Mason",
		"Ethan",
		"Caden",
		"Jacob",
		"Logan",
		"Samantha",
		"Vanessa",
		"Sophia",
		"Emma",
		"Olivia",
		"Ava",
		"Isabella",
		"Mia",
		"Zoe",
		"Lily",
		"Emily",
		"Madelyn"
	},
	last = {
		"Roope",
		"Frank",
		"Blackburn",
		"Harrington",
		"Smith",
		"Jones",
		"Williams",
		"Brown",
		"Wilson",
		"Taylor",
		"Nguyen",
		"Johnson",
		"Martin",
		"White",
		"Anderson",
		"Walker",
		"Thompson",
		"Thomas",
		"Lee",
		"Harris",
		"Ryan",
		"Robinson",
		"Kelly",
		"King",
	}
}
