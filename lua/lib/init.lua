-- chikun :: 2015
-- Load all libraries


require(... .. "/bindings")		-- Shorthand bindings
require(... .. "/class")		-- Class system

require(... .. "/click")		-- Click manager
require(... .. "/maths")		-- Extra maths functions
require(... .. "/name")			-- Random name generator
require(... .. "/states")		-- State manager
