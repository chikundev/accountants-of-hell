-- chikun :: 2015
-- Base class for all fears


--[[
 	Create the general fear class
 	INPUT:	Name and description of fear.
 	OUTPUT:	Newly created fear.
]]
Fear = class(function(new_fear, name, description, IFL)

		new_fear.name = name
		new_fear.description = description
		new_fear.IFL = IFL
	end)


--[[
	Returns the name of the fear
	INPUT:	Nothing.
	OUTPUT:	Name of fear.
]]
function Fear:getName()

	return self.name
end


--[[
	Returns the description of the fear
	INPUT:	Nothing.
	OUTPUT:	Description of the fear.
]]
function Fear:getDescription()

	return self.description
end
