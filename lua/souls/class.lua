-- chikun :: 2015
-- Base class for all souls


--[[
	Create the general soul class
	INPUT:	name of soul.
	OUTPUT:	Newly created soul.
]]
SoulClass = class(function(new_soul, name, impurity)

		new_soul.name = name
		new_soul.impurity = impurity

		-- Blank tables to hold soul's sensitivities
		new_soul.sensitivity = { }
		new_soul.owner = "Player"
		new_soul.time = math.random(50, 60)

		-- Generate our soul's traits
		new_soul:generateTraits()
	end)


--[[
	Generates the traits for a soul
	INPUT:	Nothing.
	OUTPUT:	Nothing.
]]
function SoulClass:generateTraits()

	--[[
	Local function to determine if a fear exists in a given table
	INPUT	Table to iterate through, name to check
	OUTPUT:	Boolean, whether or not the name was found in table
	]]
	local fearExists = function(trait_table, name)

		-- Iterate through the table
		for key, fear_soul in ipairs(trait_table) do

			-- If fear is found, return true
			if (fear[name] == fear_soul.type) then

				return true
			end
		end

		-- Otherwise return false
		return false
	end

	-- Generate fears and likes
	repeat

		-- Choose new possiblility
		new_sensitivity_name = fear_names[math.random(1, #fear_names)]

		-- Check if already used
		if not (fearExists(self.sensitivity, new_sensitivity_name)) then

			-- Add to sensitivities with a random strength
			table.insert(self.sensitivity, {
					type       = fear[new_sensitivity_name],
					level      = math.random(-5, 5),
					exp        = 0,
					active     = false,
					active_for = 0,
					modifiers  = { }
				})
		end
	until (#self.sensitivity == 5)
end


--[[
	Returns the name of the soul
	INPUT:	Nothing.
	OUTPUT:	Name of soul.
]]
function SoulClass:getName()

	return self.name
end


--[[
	Returns the impurity of the soul
	INPUT:	Nothing.
	OUTPUT:	Impurity of soul.
]]
function SoulClass:getImpurity()

	return self.impurity
end
