-- chikun :: 2015
-- Soul controller


-- Controller of souls
soul = {

	-- List of all souls
	list = { }
}


--[[
	Create a soul and add it to the list of souls
	INPUT: 	name of new soul.
			impurities level of new soul.
	OUTPUT: The new soul.
]]
function soul.add(name, impurity, owner)

	-- Create new soul with given name and impurity
	local new_soul = SoulClass(name, impurity)

	-- Insert the new soul into our list
	for key, players in ipairs(player.list) do
		if (players.name == owner) then
			table.insert(player.list[key].souls, new_soul)
		end
	end

	-- Return the new soul
	return new_soul
end


-- Update function for soul
-- Input: delta time
-- Output: nothing
function soul.update(dt)

	-- Going through all players that exist
	for key, players in ipairs(player.list) do

		-- For all souls that player owns
		for key, soul in ipairs(players.souls) do

			-- Local variables to hold important data
			local level_difference, IFL_multiplier, money_earnt = 0, 0, 0

			-- For each sensitivity on a soul
			for key, sensitivity in ipairs(soul.sensitivity) do

				-- If the sensitivity is active
				if (sensitivity.active) then

					-- Add the level to the current level. Up if fear down if like
					level_difference = level_difference + sensitivity.level

					-- Calculate multiplier for the fear
					IFL_multiplier = IFL_multiplier +
					                 math.abs(sensitivity.level) *
					                 sensitivity.type.IFL
				end
			end

			level_difference = math.sign(level_difference, true)

			-- Calculate money earnt
			money_earnt = IFL_multiplier* 10 * dt * level_difference

			-- Add money to players inventory
			players.money = players.money + money_earnt

			-- Add variable to determine if soul has earnt money or not
			soul.earn_money = math.sign(money_earnt, false)

			-- Going through each sensitivity on a soul again
			for key, sensitivity in ipairs(soul.sensitivity) do

				-- If the sensitivity is active
				if (sensitivity.active) then

					-- Make the length of time a sensitivity is active for go up
					sensitivity.active_for = sensitivity.active_for + dt / 10

					-- Add on experience to the sensitivity
					sensitivity.exp = sensitivity.exp + money_earnt -
					                  sensitivity.active_for

					sensitivity.exp_dif = math.sign(money_earnt -
												sensitivity.active_for, false)

					-- If sensitivity breaches a limit
					if (sensitivity.exp >=
						(math.abs(sensitivity.level) + 1) * 1000 or
						sensitivity.exp <=
						(math.abs(sensitivity.level) + 1) * -1000) then

						-- If sensitivity has breached upper limit
						if (sensitivity.exp > 0) then

							-- If sensitivity is below maximum
							if sensitivity.level < 5 then
								-- Sensitivity goes up by one
								sensitivity.level = sensitivity.level + 1
							else
								-- Set exp to maximum
								sensitivity.exp = (sensitivity.level + 1) * 1000
							end
						else

							if sensitivity.level > -5 then
								-- Sensitivity goes down by one
								sensitivity.level = sensitivity.level - 1
							else
								sensitivity.level = (sensitivity.level + 1) * -1000
							end
						end

						-- Reset experience
						sensitivity.exp = 0
					end
				else

					-- If not active then decrease time active for
					sensitivity.active_for = sensitivity.active_for - dt * 2

					sensitivity.exp_dif = 0

					-- If active for becomes negative
					if (sensitivity.active_for < 0) then

						-- Make it zero
						sensitivity.active_for = 0
					end
				end
			end
		end
	end
end


--[[
	Remove an impurity from the souls
	INPUT:	Delta time.
	OUTPUT:	Nothing.
]]
function soul.countDown(dt)

	-- run through list of all players
	for key, player in ipairs(player.list) do

		-- Run through all souls in each player
		for key2, timed_soul in ipairs(player.souls) do

			-- decrease souls time
			timed_soul.time = timed_soul.time - dt * 1

			-- when souls time runs out
			if (timed_soul.time <= 0) then

				-- reset souls time
				timed_soul.time = timed_soul.time + math.random(50, 60)

				-- decrease amount of impurities
				timed_soul.impurity = timed_soul.impurity - 1

				-- when impurityies run out
				if (timed_soul.impurity <= 0) then

					-- remove soul from table
					table.remove(player.souls, key2)

					-- if player who lost soul was main player
					if (key == 1) then

						-- if in the souls door
						if (current_soul == timed_soul) then

							-- Force state to play
							state.set(states.play)
						end

						-- update where doors are positioned
						soul.updateDoors()
					end
				end
			end
		end
	end
end

--[[
	Reposition all souls doors
	Input:  Nothing
	Output: Nothing
]]
function soul.updateDoors()

	for key, soul in ipairs(player.list[1].souls) do
		local x_pos = (key - 1) % 5
		local y_pos = math.floor((key - 1) / 5)

		soul.object.x = 150 + (250 * x_pos)
		soul.object.y = 714 - (155 * (y_pos + 1))
	end
end
