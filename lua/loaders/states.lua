-- chikun :: 2014-2015
-- Loads all states from the /src/states folder


--[[
	Recursively checks a folder for states and adds any found to a state.list
	table.
	INPUT:	Directory and table to load from and to.
	OUTPUT:	Table which has been loaded.
]]
function checkFolder(dir, tab)

	local tab = (tab or { })

	-- Get a table of files / subdirs from dir
	local items = lf.getDirectoryItems(dir)

	for key, val in ipairs(items) do

		-- If item is a file, then load it into tab
		if (lf.isFile(dir .. "/" .. val)) then

			-- Remove ".lua" extension on file
			local name = val:gsub(".lua", "")

			-- Load state into table
			tab[name] = require(dir .. "/" .. name)

		-- Otherwise, run checkFolder on subdir
		else

			-- Add new table onto tab
			tab[val] = { }

			-- Run checkFolder in this subdir
			checkFolder(dir .. "/" .. val, tab[val])
		end
	end

	return tab
end


-- Load the src/states folder into the states table
states = checkFolder("lua/states")

-- Kills checkFolder
checkFolder = nil
