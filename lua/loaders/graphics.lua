-- chikun :: 2014-2015
-- Loads all graphics from the /gfx folder


--[[
	Recursively checks a folder for graphics and adds any found to a gfx table.
	INPUT:	Directory and table to load from and to.
	OUTPUT:	Table which has been loaded.
]]
function checkFolder(dir, tab)

	local tab = ( tab or { } )

	-- Get a table of files / subdirs from dir
	local items = lf.getDirectoryItems(dir)

	for key, val in ipairs(items) do

		-- If item is a file, then load it into tab
		if lf.isFile(dir .. "/" .. val) then

			-- As long as it's not a Thumbs.db
			if val ~= "Thumbs.db" then

				-- Remove extension on file
				local name = val:sub(1, val:len() - 4)

				-- Load image into table
				tab[name] = lg.newImage(dir .. "/" .. val)
			end

		-- Else, run checkFolder on subdir
		else

			-- Add new table onto tab
			tab[val] = { }

			-- Run checkFolder in this subdir
			checkFolder(dir .. "/" .. val, tab[val])
		end
	end

	return tab
end


-- Load the gfx folder into the gfx table
gfx = checkFolder("gfx")

-- Kills checkFolder
checkFolder = nil
