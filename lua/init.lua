-- chikun :: 2015
-- Load game code


require(... .. "/lib")      	-- Libraries :: IMPORTANT AS FIRST

require(... .. "/fears")    	-- Fears
require(... .. "/loaders")  	-- Loaders of resources
require(... .. "/souls")    	-- Souls
require(... .. "/players")		-- Players
