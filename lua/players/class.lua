-- chikun :: 2015
-- Base class for all players


--[[
	Create the general player class
	INPUT:	name of player.
	OUTPUT:	Newly created player.
]]
PlayerClass = class(function(new_player, name)

		-- spawning in new player
		new_player.name = name
		new_player.money = -1000000
		new_player.mods = { }
		new_player.souls = { }
	end)

--[[
	Returns the name of the player
	INPUT:	Nothing.
	OUTPUT:	Name of player.
]]
function PlayerClass:getName()

	return self.name
end


--[[
	Returns the impurity of the Player
	INPUT:	Nothing.
	OUTPUT:	Money of Player.
]]
function PlayerClass:getMoney()

	return self.money
end
