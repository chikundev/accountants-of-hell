-- chikun :: 2015
-- player controller


-- Controller of players
player = {

	-- List of all players
	list = { }
}

--[[
	Create a player and add it to the list of players
	INPUT: 	name of new player.
	OUTPUT: The new player.
]]
function player.add(name)

	-- Create new player
	local new_player = PlayerClass(name)

	-- Insert the new player into our list
	table.insert(player.list, new_player)

	-- Return the new player
	return new_player
end

--[[
	runs an update for all players
	Input: Delta time
	Output: nil
]]
function player.update(dt) end
