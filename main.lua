-- chikun :: 2015
-- Accountants of Hell


-- If the game is pixel based, set nearest filter
if (IS_PIXEL_BASED) then love.graphics.setDefaultFilter('nearest') end


-- Load all libraries
require("lua")


function love.load()

	-- Scale new canvas and get scale values
	game_canvas = lg.newCanvas(GAME_WIDTH, GAME_HEIGHT)
	off_x, off_y, scale = getScale(lg.getWidth(), lg.getHeight())

	-- Load play state
	state.load(states.play)
end


function love.update(dt)

	-- add in a speed factor to dt
	dt = dt * speed_factor

	-- Restrict dt
	dt = math.min(dt, 1/10)

	-- Update current state
	state.update(dt)
end


function love.draw()

	-- Draw current state to game canvas after clearing it
	lg.clear()
	lg.setCanvas(game_canvas)
	state.draw()
	lg.setCanvas()

	-- Actually draw game_canvas
	lg.setColor(255, 255, 255)
	lg.draw(game_canvas, off_x, off_y, 0, scale)
end


--[[
	Simple keyboard buttons, mostly for debug.
	INPUT:  Key which has been pressed.
]]
function love.keypressed(key)

	if (key == "f11") then

		lw.setFullscreen(not lw.getFullscreen())
	elseif (key == "escape") then

		le.quit()
	end
end


--[[
	Run upon game resize.
	INPUT:  width and height are the dimensions of the resized window.
]]
function love.resize(width, height)

	-- Recalculate canvas scaling
	off_x, off_y, scale = getScale(width, height)
end


--[[
	Returns details for canvas scaling.
	INPUT:  width and height are the dimensions of the game window.
	OUTPUT: off_x and off_y are offset parameters for canvas x and y.
	        scale is the scaling value for the canvas.
]]
function getScale(width, height)

	-- Calculate horizontal and vertical scale values
	local scale_w, scale_h = (width / GAME_WIDTH), (height / GAME_HEIGHT)

	-- Calculate offsets based on scale values
	local off_x, off_y = math.max((scale_w - scale_h) * GAME_WIDTH / 2, 0),
	                     math.max((scale_h - scale_w) * GAME_HEIGHT / 2, 0)

	-- Return calculated offsets and the minimum scale value
	return off_x, off_y, math.min(scale_w, scale_h)
end
