-- chikun :: 2015
-- Configuration file for Accountants of Hell

require("flags")  -- File containing custom flags for game

function love.conf(game)

	game.identity = "Accountants of Hell"
	game.version = "0.9.2"
	game.console = true      -- Set to false on distribution

	-- Omit modules due to disuse
	game.modules.math    = false
	game.modules.physics = false
	game.modules.thread  = false

	-- Window arguments
	game.window.title          = "Accountants of Hell"
	game.window.icon           = "gfx/system/icon.png"
	game.window.resizable      = true
	game.window.width          = GAME_WIDTH * DEFAULT_SCALE
	game.window.height         = GAME_HEIGHT * DEFAULT_SCALE
	game.window.minwidth       = GAME_WIDTH
	game.window.minheight      = GAME_HEIGHT
	game.window.fullscreen     = FULLSCREEN
	game.window.fullscreentype = "desktop"
	game.window.vsync          = GAME_VSYNC
end
