-- chikun :: 2015
-- Game flags


DEFAULT_SCALE = 1
FULLSCREEN    = false
GAME_VSYNC    = true
GAME_WIDTH    = 1280
GAME_HEIGHT   = 720

IS_OUYA        = false  -- Whether or not the game is running on an OUYA
IS_PIXEL_BASED = false   -- Whether or not the game is pixel based


-- Load test flags is they exist on the system
if (love.filesystem.exists("test_flags.lua")) then require("test_flags") end
